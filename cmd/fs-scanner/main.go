package main

import "gitlab.com/andrewheberle/fs-scanner/internal/pkg/cmd"

func main() {
	cmd.Execute()
}
