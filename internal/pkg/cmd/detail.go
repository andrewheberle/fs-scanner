package cmd

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"time"

	"github.com/djherbis/atime"
	"github.com/spf13/cobra"
)

func init() {
	// Add flags
	detailCmd.Flags().StringVarP(&scanPath, "path", "p", ".", "Path to scan")
	detailCmd.Flags().StringVarP(&outFile, "out", "o", "", "Output file (default is stdout)")
	detailCmd.Flags().StringVarP(&sizeUnit, "unit", "u", "b", "Size unit: b, k, m, g")
	detailCmd.Flags().BoolVarP(&excelSerialDate, "excelserial", "e", false, "Format dates as Excel serial numbers")

	// Add command
	rootCmd.AddCommand(detailCmd)
}

var (
	// Flags
	scanPath, outFile, sizeUnit string
	excelSerialDate             bool

	// Command
	detailCmd = &cobra.Command{
		Use:   "detail",
		Short: "Run a detailed scan",
		Long:  "This scans a path and returns size, file count and atime/mtime values for all directories",
		Run: func(cmd *cobra.Command, args []string) {
			if err := doDetailed(); err != nil {
				fmt.Fprintf(os.Stderr, "Problem with detail: %s\n", err)
			}
		},
	}

	scanInfo = make(map[string]scanData)
)

type scanData struct {
	mtime time.Time
	atime time.Time
	size  int64
	files int64
	dirs  int64
}

func toExcelSerialDate(t time.Time) int64 {
	epoch, err := time.Parse("2/1/2006 00:00:00", "1/1/1900 00:00:00")
	if err != nil {
		return 0
	}

	return int64(t.Sub(epoch).Hours()) / 24
}

func doDetailed() error {
	var f *os.File
	var unitFunc func(n int64) float64

	// ensure scan path exists
	if _, err := os.Stat(scanPath); os.IsNotExist(err) {
		return err
	}

	// use different function depending on unit
	switch sizeUnit {
	case "b":
		unitFunc = func(n int64) float64 {
			return float64(n)
		}
	case "k":
		unitFunc = func(n int64) float64 {
			return float64(n) / 1024.0
		}
	case "m":
		unitFunc = func(n int64) float64 {
			return (float64(n) / 1024.0) / 1024.0
		}
	case "g":
		unitFunc = func(n int64) float64 {
			return ((float64(n) / 1024.0) / 1024.0) / 1024.0
		}
	case "t":
		unitFunc = func(n int64) float64 {
			return (((float64(n) / 1024.0) / 1024.0) / 1024.0) / 1024.0
		}
	default:
		return ErrorInvalidUnit
	}

	err := filepath.WalkDir(filepath.Clean(scanPath), func(path string, d fs.DirEntry, err error) error {
		// continue regardless of error
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err)
			return nil
		}

		info, err := d.Info()
		if err != nil {
			// skip
			return nil
		}

		// if path is dir add to map and set mtime
		if info.IsDir() {
			scanInfo[path] = scanData{
				mtime: info.ModTime(),
				atime: atime.Get(info),
			}
		}

		// dont update parent info at root of scan
		if path == filepath.Clean(scanPath) {
			return nil
		}

		// update parent details and propagate upwards
		thispath := path
		for {
			// if we are the top level bail out
			if thispath == filepath.Clean(scanPath) {
				break
			}

			// work out parent path
			parentPath := filepath.Dir(thispath)

			// check if parent exists
			if parent, found := scanInfo[parentPath]; found {
				// update mtime of parent if required
				if info.ModTime().After(parent.mtime) {
					parent.mtime = info.ModTime()
				}

				// update atime of parent if required
				if atime.Get(info).After(parent.atime) {
					parent.atime = atime.Get(info)
				}

				// add size
				parent.size = parent.size + info.Size()
				if info.IsDir() {
					parent.dirs++
				} else {
					parent.files++
				}

				// update parent info
				scanInfo[parentPath] = parent
			} else {
				// set parent info directly from scratch based on this file
				if d.IsDir() {
					scanInfo[parentPath] = scanData{
						mtime: info.ModTime(),
						atime: atime.Get(info),
						size:  info.Size(),
						dirs:  1,
						files: 0,
					}
				} else {
					scanInfo[parentPath] = scanData{
						mtime: info.ModTime(),
						atime: atime.Get(info),
						size:  info.Size(),
						files: 1,
						dirs:  0,
					}
				}
			}

			// update thispath for next loop
			thispath = parentPath
		}

		return nil
	})

	// show if an error occurred with the walk
	if err != nil {
		return err
	}

	// create output file
	if outFile == "" {
		f = os.Stdout
	} else {
		f, err = os.Create(outFile)
		if err != nil {
			return err
		}
		defer f.Close()
	}

	// output results
	if excelSerialDate {
		fmt.Fprintf(f, "path,size_%s,files,dirs,mdate,adate\n", sizeUnit)
	} else {
		fmt.Fprintf(f, "path,size_%s,files,dirs,mtime,atime\n", sizeUnit)
	}
	for k, v := range scanInfo {
		if excelSerialDate {
			fmt.Fprintf(f, "\"%s\",%.2f,%d,%d,%d,%d\n", k, unitFunc(v.size), v.files, v.dirs, toExcelSerialDate(v.mtime), toExcelSerialDate(v.atime))
		} else {
			fmt.Fprintf(f, "\"%s\",%.2f,%d,%d,%s,%s\n", k, unitFunc(v.size), v.files, v.dirs, v.mtime, v.atime)
		}
	}

	return nil
}
