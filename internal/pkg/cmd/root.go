package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "fs-scanner",
	Short: "File System Scanner",
	Long:  "This will scan a path and provide a report on files, sizes and types.",
}

var (
	ErrorInvalidUnit = fmt.Errorf("invalid unit specified")
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
