package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/andrewheberle/fs-scanner/internal/pkg/metadata"
)

func init() {
	// Add flags
	summaryCmd.Flags().StringVarP(&p, "path", "p", ".", "Path to scan")
	summaryCmd.Flags().StringVarP(&u, "unit", "u", "h", "Unit for sizes (h, k, m, g, t, p)")
	summaryCmd.Flags().BoolVar(&debug, "debug", false, "Enable debug mode")

	// Add command
	rootCmd.AddCommand(summaryCmd)
}

var (
	// Flags
	p, u  string
	debug bool

	// Command
	summaryCmd = &cobra.Command{
		Use:   "summary",
		Short: "Show a file system summary",
		Long:  "This scans a path and returns a summary about file types and mtime/atime details.",
		Run: func(cmd *cobra.Command, args []string) {
			if err := doSummary(); err != nil {
				fmt.Fprintf(os.Stderr, "Problem with summary: %s\n", err)
			}
		},
	}
)

func doSummary() error {
	var unit metadata.ByteSize

	m := metadata.NewMetadata(debug)

	// validate unit
	switch u {
	case "h":
		unit = metadata.HumanReadable
	case "k":
		unit = metadata.KB
	case "m":
		unit = metadata.MB
	case "g":
		unit = metadata.GB
	case "t":
		unit = metadata.TB
	case "p":
		unit = metadata.PB
	default:
		return ErrorInvalidUnit
	}

	err := filepath.WalkDir(p, m.WalkDirFunc)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
	}

	tnum := int64(0)
	tsize := metadata.ByteSize(0)
	fmt.Printf("=====================\n")
	fmt.Printf("By File Type\n")
	fmt.Printf("=====================\n")
	fmt.Printf("Type,# Files,Size\n")
	for ext, filemeta := range m.Extension {
		tnum = tnum + filemeta.Count
		tsize = tsize + filemeta.Size
		if unit == metadata.HumanReadable {
			fmt.Printf("\"%s\",%v,%v\n", ext, filemeta.Count, filemeta.Size)
		} else {
			fmt.Printf("\"%s\",%v,%.2f\n", ext, filemeta.Count, filemeta.Size.ToUnit(unit))
		}
	}
	fmt.Printf("=====================\n")
	if unit == metadata.HumanReadable {
		fmt.Printf("Total,%v,%s\n\n", tnum, tsize)
	} else {
		fmt.Printf("Total,%v,%.2f\n\n", tnum, tsize.ToUnit(unit))
	}

	fmt.Printf("=====================\n")
	fmt.Printf("By File Modification Age\n")
	fmt.Printf("=====================\n")
	fmt.Printf("Modified Within,# Files,Size\n")
	tnum = 0
	tsize = 0
	for mtime := metadata.OneWeek; mtime <= metadata.Older; mtime++ {
		filemeta := m.ModificationTime[mtime]
		tnum = tnum + filemeta.Count
		tsize = tsize + filemeta.Size
		if unit == metadata.HumanReadable {
			fmt.Printf("%s,%v,%s\n", mtime, filemeta.Count, filemeta.Size)
		} else {
			fmt.Printf("%s,%v,%.2f\n", mtime, filemeta.Count, filemeta.Size.ToUnit(unit))
		}
	}
	fmt.Printf("=====================\n")
	if unit == metadata.HumanReadable {
		fmt.Printf("Total,%v,%s\n\n", tnum, tsize)
	} else {
		fmt.Printf("Total,%v,%.2f\n\n", tnum, tsize.ToUnit(unit))
	}

	fmt.Printf("=====================\n")
	fmt.Printf("By File Access Age\n")
	fmt.Printf("=====================\n")
	fmt.Printf("Accessed Within,# Files,Size\n")
	tnum = 0
	tsize = 0
	for atime := metadata.OneWeek; atime <= metadata.Older; atime++ {
		filemeta := m.AccessTime[atime]
		tnum = tnum + filemeta.Count
		tsize = tsize + filemeta.Size
		if unit == metadata.HumanReadable {
			fmt.Printf("%s,%v,%s\n", atime, filemeta.Count, filemeta.Size)
		} else {
			fmt.Printf("%s,%v,%.2f\n", atime, filemeta.Count, filemeta.Size.ToUnit(unit))
		}
	}
	fmt.Printf("=====================\n")
	if unit == metadata.HumanReadable {
		fmt.Printf("Total,%v,%s\n\n", tnum, tsize)
	} else {
		fmt.Printf("Total,%v,%.2f\n\n", tnum, tsize.ToUnit(unit))
	}

	return nil
}
