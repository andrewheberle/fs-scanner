package metadata

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/djherbis/atime"
)

// Extra time.Duration constants
const (
	Week  time.Duration = (time.Hour * 24) * 7
	Month               = Year / 12
	Year                = Week * 52
)

// ByteSize is our bytes value
type ByteSize int64

// ByteSize constants
const (
	_           = iota // ignore first value by assigning to blank identifier
	KB ByteSize = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	HumanReadable ByteSize = 1
)

type FileStat struct {
	Count int64
	Size  ByteSize
}

type ExtMap map[Ext]FileStat
type AgeMap map[Age]FileStat

type Metadata struct {
	Extension        ExtMap
	ModificationTime AgeMap
	AccessTime       AgeMap
	debug            bool
}

// Age type
type Age int

// Age constant
const (
	OneWeek Age = iota
	TwoWeeks
	ThreeWeeks
	OneMonth
	ThreeMonths
	SixMonths
	OneYear
	TwoYears
	Older
)

// Ext type
type Ext int

// Ext constants
const (
	Documents Ext = iota
	Multimedia
	Data
	Database
	Executable
	Internet
	Archive
	Temp
	Config
	Log
	Backup
	System
	VM
	Development
	Email
	Other
)

func extensionLookup(ext string) Ext {
	switch strings.ToLower(ext) {
	case ".docx", ".docm", ".dotx", ".dotm", ".doc", ".dot", ".odt", ".rtf", ".wps", // Word processing Documents
		".xlsx", ".xlsm", ".xlsb", ".xltx", ".xltm", ".xls", ".xlt", ".ods", ".csv", // Spreadsheets
		".pptx", ".pptm", ".potx", ".potm", ".ppt", ".pot", ".otp", // Presentations
		".pub", ".pdf", ".xps", ".ps", ".svg", ".eps", // Desktop Publishing
		".vsd", ".vss", ".vsdx", ".dia", // Diagrams
		".md", ".markdown", ".txt", ".text", // Text formats
		".dwg", ".dws", ".dwt", // CAD
		".lbl", ".rpt", ".oft", ".mpp":
		return Documents
	case ".eml", ".pst", ".nsf", ".msg", ".ost":
		return Email
	case ".avi", ".mp4", ".vob", ".mpg", ".mpeg", ".mkv", ".ts", ".flv", ".wmv", ".swf", ".mov", // Video Files
		".mp3", ".mp4a", ".m4a", ".ape", ".flaac", ".wav", ".ac3", ".wma", ".gsm", ".m3u", ".ai", ".mid", // Audio files
		".bmp", ".bpg", ".ecw", ".gif", ".jpg", ".jpeg", ".jpe", ".png", // Images
		".psd", ".pgm", ".ppm", ".tif", ".tiff", ".raw", ".xcf", ".ico":
		return Multimedia // Images (cont.)
	case ".dll", ".ocx", ".so", ".a", ".o", // Libraries and modules
		".exe", ".com", // Executables
		".sh", ".bat", ".cmd", ".ps1", ".expect", ".expect-noinput", ".vbs", // Scripts
		".hlp", ".chm", ".msp", ".jar":
		return Executable
	case ".zip", ".arc", ".arj", ".7z", ".rar",
		".tar", ".gz", ".tgz", ".bz", ".bz2", ".xz",
		".cab", ".rpm", ".deflate", ".msi", ".apt", ".pkg", ".dmg", ".pack", ".iso":
		return Archive
	case ".sys", ".ko", ".lnk", ".inf", ".cat", ".nfo", ".pol", ".adm", ".ttf", ".manifest", ".reg", ".icm", ".msc", ".cpl", ".cnt", ".dns":
		return System
	case ".vmdk", ".vmx", ".vmem", ".vmsd", ".vmsn", ".vmss", ".vmtm", ".vmfx", // VMware
		".ova", ".ovf", // OVF
		".vhd", ".vsv", ".avhd", // Hyper-V
		".vbox", ".sav", // Virtual Box
		".xva", // Xen
		".qcow2", ".qcow":
		return VM // KVM/QEMU
	case ".htm", ".html", ".css", ".js", ".json", ".mht", ".mhtml", ".xhtm", ".xhtml", ".xml", ".url",
		".php", ".php5", ".php4", ".asp", ".aspx":
		return Internet
	case ".cfg", ".cnf", ".conf", ".config", ".ini", ".yml", ".yaml", ".pp", ".opts", ".service", ".rdp", ".lic", ".mst", ".ahk", ".htaccess", ".env":
		return Config
	case ".db", ".mdb", ".edb", ".mdf", ".ldf", ".fdb", ".gdb", ".sqlite", ".sqlite3", ".sql", ".db3", ".dbf", ".ibd", ".tdb", ".odb", ".mdt", ".idx":
		return Database
	case ".log":
		return Log
	case ".tmp", ".dump", ".core", ".exe~", ".swp", ".vim", ".~vsd":
		return Temp
	case ".bak", ".backup":
		return Backup
	case ".dat", ".bin", ".img", ".gpg", ".sample", ".tmpl", ".template", ".key", ".ppk", ".pfx", ".crt", ".der", ".cer", ".pem", ".md5",
		".pcap", ".pcapng", ".adt":
		return Data
	case ".c", ".h", ".in",
		".go", ".proto", // Golang
		".git", ".gitattributes", ".gitignore", ".cvsignore", // SCM
		".rb", ".erb", ".epp", ".yardopts", // Ruby
		".py", ".pyc", ".pyd", // Python
		".vagrant",
		".pl", // Perl
		".vscode", ".s", ".inc", ".module",
		".golden",
		".spec", ".rpmmacros", ".patch", ".diff", ".class":
		return Development
	}
	// fmt.Printf("Unclassified Extension: %v\n", ext)
	return Other
}

func (b ByteSize) ToUnit(u ByteSize) float64 {
	switch u {
	case PB:
		return float64(b) / float64(PB)
	case TB:
		return float64(b) / float64(TB)
	case GB:
		return float64(b) / float64(GB)
	case MB:
		return float64(b) / float64(MB)
	case KB:
		return float64(b) / float64(KB)
	}

	return float64(b)
}

func (b ByteSize) String() string {
	switch {
	case b >= PB:
		return fmt.Sprintf("%.2fPB", float64(b)/float64(PB))
	case b >= TB:
		return fmt.Sprintf("%.2fTB", float64(b)/float64(TB))
	case b >= GB:
		return fmt.Sprintf("%.2fGB", float64(b)/float64(GB))
	case b >= MB:
		return fmt.Sprintf("%.2fMB", float64(b)/float64(MB))
	case b >= KB:
		return fmt.Sprintf("%.2fKB", float64(b)/float64(KB))
	}
	return fmt.Sprintf("%.2fB", float64(b))
}

func (a Age) String() string {
	switch a {
	case OneWeek:
		return "One Week"
	case TwoWeeks:
		return "Two Weeks"
	case ThreeWeeks:
		return "Three Weeks"
	case OneMonth:
		return "One Month"
	case ThreeMonths:
		return "Three Months"
	case SixMonths:
		return "Six Months"
	case OneYear:
		return "One Year"
	case TwoYears:
		return "Two Years"
	case Older:
		return "Older Than Two Years"
	}
	return "Other"
}

func (e Ext) String() string {
	switch e {
	case Documents:
		return "Document Files"
	case Multimedia:
		return "Multimedia Files"
	case Data:
		return "Data Files"
	case Database:
		return "Databases"
	case Executable:
		return "Executables, Programs and Scripts"
	case Internet:
		return "Internet Files"
	case Archive:
		return "Compressed Files and Archives"
	case Temp:
		return "Temporary Files"
	case Config:
		return "Configuration Files"
	case Log:
		return "Log Files"
	case Backup:
		return "Backup Files"
	case System:
		return "System Files"
	case VM:
		return "Virtual Machine Disk Image or File"
	case Development:
		return "Development and Programming Files"
	case Email:
		return "Email Files/Archives"
	}
	return "Other"
}

func (f FileStat) Increment(s int64) FileStat {
	return FileStat{
		Count: f.Count + 1,
		Size:  f.Size + ByteSize(s),
	}
}

func NewMetadata(debug bool) *Metadata {
	return &Metadata{
		Extension:        make(ExtMap),
		AccessTime:       make(AgeMap),
		ModificationTime: make(AgeMap),
		debug:            debug,
	}
}

func (m *Metadata) WalkDirFunc(p string, d fs.DirEntry, err error) error {
	if m.debug {
		fmt.Fprintf(os.Stderr, "Debug: %s\n", d.Name())
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		return fs.SkipDir
	}
	if d.IsDir() {
		// Do nothing if this is a directory
		return nil
	}
	info, err := d.Info()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		return nil
	}
	// Get file size
	size := info.Size()
	// Get extension from filename
	ext := extensionLookup(filepath.Ext(info.Name()))
	// Increment size and count for extension
	m.Extension[ext] = m.Extension[ext].Increment(size)

	// Get key for age
	mtimeKey := getAgeKey(time.Since(info.ModTime()))
	atimeKey := getAgeKey(time.Since(atime.Get(info)))

	// Increment size and count for age of file
	m.ModificationTime[mtimeKey] = m.ModificationTime[mtimeKey].Increment(size)
	m.AccessTime[atimeKey] = m.AccessTime[atimeKey].Increment(size)

	return nil
}

func getAgeKey(d time.Duration) (agekey Age) {
	agekey = Older
	// Set key based on mtime
	switch {
	case d <= Week:
		agekey = OneWeek
	case d <= Week*2:
		agekey = TwoWeeks
	case d <= Week*3:
		agekey = ThreeWeeks
	case d <= Month:
		agekey = OneMonth
	case d <= Month*3:
		agekey = ThreeMonths
	case d <= Month*6:
		agekey = SixMonths
	case d <= Year:
		agekey = OneYear
	case d <= Year*2:
		agekey = TwoYears
	}

	return agekey
}
